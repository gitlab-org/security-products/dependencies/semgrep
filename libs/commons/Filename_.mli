(* Deprecated: use the Ppath module instead! *)
val filename_without_leading_path :
  string -> string (* filename *) -> string (* filename *)

val readable : root:string -> string (* filename *) -> string (* filename *)
